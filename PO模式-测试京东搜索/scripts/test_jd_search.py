"""
    测试京东搜索
"""
import unittest
import time

from selenium import webdriver
from page.home_page import HomePage
from page.search_result_page import SearchResultPage


class TestJDSearch(unittest.TestCase):

    def setUp(self) -> None:
        # 创建浏览器驱动对象
        options = webdriver.ChromeOptions()
        options.binary_location = r"C:\Users\李阳\AppData\Local\CentBrowser\Application\chrome.exe"
        self.driver = webdriver.Chrome(chrome_options=options)
        # 窗口最大化
        self.driver.maximize_window()
        # 设置隐式等待，防止元素没有准备好
        self.driver.implicitly_wait(10)
        self.driver.get("https://www.jd.com/")

        # 创建首页对象
        self.home_page = HomePage(self.driver)
        # 创建搜索结果页对象
        self.search_result_page = SearchResultPage(self.driver)

    def tearDown(self) -> None:
        time.sleep(3)
        self.driver.quit()

    # 公共测试方法
    def common_test_method(self, search_value):
        self.home_page.test_JD_search_label(search_value)
        self.home_page.click_search_button()
        time.sleep(0.5)
        assert self.search_result_page.get_search_value().strip('"') == search_value

    # 测试搜索手机
    def test_search_phone(self):
        self.common_test_method("手机")

    # 测试搜索电脑
    def test_search_window(self):
        self.common_test_method("电脑")
