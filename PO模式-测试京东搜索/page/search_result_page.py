"""
    搜索结果页面
"""
from selenium.webdriver.common.by import By


class SearchResultPage:
    # 搜索的内容标签
    search_content_label = By.XPATH, "//strong[@class='search-key']"

    def __init__(self, driver):
        self.driver = driver

    # 获取搜索的内容
    def get_search_value(self):
        return self.driver.find_element(*self.search_content_label).text
