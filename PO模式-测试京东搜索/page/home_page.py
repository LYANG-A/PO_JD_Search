"""
    主页
"""
from selenium.webdriver.common.by import By


class HomePage:
    # 输入框元素
    find_element_label = By.ID, "key"
    # 搜索按钮元素
    search_button_label = By.XPATH, "//button[@aria-label='搜索']"

    def __init__(self, driver):
        self.driver = driver

    # 搜索
    def test_JD_search_label(self, search_value):
        self.driver.find_element(*self.find_element_label).send_keys(search_value)

    # 点击搜索按钮
    def click_search_button(self):
        self.driver.find_element(*self.search_button_label).click()
