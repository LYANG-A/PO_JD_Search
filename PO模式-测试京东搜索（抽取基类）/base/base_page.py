class BasePage(object):

    def __init__(self, driver):
        self.driver = driver

    # 定位单个元素标签
    def find_element(self, location_info):
        return self.driver.find_element(*location_info)

    # 定位多个元素标签
    def find_elements(self, location_info):
        return self.driver.find_elements(*location_info)

    # 输入内容
    def input_text(self, location_info, text):
        self.find_element(location_info).send_keys(text)

    # 点击元素
    def click_element(self, location_info):
        self.find_element(location_info).click()

    # 获取文本内容
    def get_text(self, location_info):
        return self.find_element(location_info).text

    # 获取属性信息
    def property_name(self, location_info, name):
        return self.find_element(location_info).get_attribute(name)

    # 清空文本内容
    def clear_text(self, location_info):
        self.find_element(location_info).clear()
