import unittest
from HTMLTestRunner import HTMLTestRunner

if __name__ == '__main__':
    test_suite = unittest.defaultTestLoader.discover("./scripts", pattern="test_*.py")
    # wb：以二进制的方式进行写入数据
    with open("./reports/report.html", "wb") as file:
        HTMLTestRunner(file, 2, "京东搜索栏测试报告", "测试环境：Window + unittest").run(test_suite)
