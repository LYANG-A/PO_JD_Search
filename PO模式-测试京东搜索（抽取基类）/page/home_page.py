"""
    主页
"""
from selenium.webdriver.common.by import By
from base.base_page import BasePage


class HomePage(BasePage):
    # 输入框元素
    find_element_label = By.ID, "key"
    # 搜索按钮元素
    search_button_label = By.XPATH, "//button[@aria-label='搜索']"

    # 搜索
    def test_JD_search_label(self, search_value):
        self.input_text(self.find_element_label, search_value)

    # 点击搜索按钮
    def click_search_button(self):
        self.click_element(self.search_button_label)
