"""
    搜索结果页面
"""
from selenium.webdriver.common.by import By
from base.base_page import BasePage


class SearchResultPage(BasePage):
    # 搜索的内容标签
    search_content_label = By.XPATH, "//strong[@class='search-key']"

    # 获取搜索的内容
    def get_search_value(self):
        return self.get_text(self.search_content_label)
